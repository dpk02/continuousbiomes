package de.dpk02.continuousbiomes.mixins;

import com.mojang.serialization.MapCodec;
import net.minecraft.core.Registry;
import net.minecraft.util.KeyDispatchDataCodec;
import net.minecraft.world.level.levelgen.DensityFunction;
import net.minecraft.world.level.levelgen.DensityFunctions;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(DensityFunctions.class)
public interface CBDensityFunctionsAccessor {

	@Invoker("makeCodec")
	static <O> KeyDispatchDataCodec<O> makeCodec(MapCodec<O> mapCodec) {
		throw new AssertionError();
	}

	@Invoker("register")
	static MapCodec<? extends DensityFunction> register(
		Registry<MapCodec<? extends DensityFunction>> registry,
		String name,
		KeyDispatchDataCodec<? extends DensityFunction> codec
	) {
		throw new AssertionError(); // Mixin will replace this at runtime
	}
}
