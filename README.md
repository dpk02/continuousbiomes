**ATTENTION: This mod is still in an alpha stage of development. Breaking changes and instability should be expected.**

Did deserts spawning right next to snowy mountains ever feel weird to you?  
Or is it the fact that the climate in Minecraft is pseudo-random, so you can never know where you may find snow in the first place?

Behold, I might have something for you:

## Extended Worldgen

This mod adds many new functions to Minecraft's Datapack system, which opens up new possibilities to developers and players alike. You can modify how the world behaves, solely based on the position of the block - to predefine a central continent no matter the seed, to have a temperature gradient in the world, and much, much more. This mod is but the framework to enable the most creative people out there to make their ideas a reality. 

## Roadmap
(in no particular order)  
- add datapacks support (see [Hijack Datapack System Instead (#2)](https://gitlab.com/dpk02/extended-worldgen/-/issues/2))

**Newer versions require the use of datapacks to function properly. An example is available [here](https://modrinth.com/datapack/extended-wordgen-example-datapack).**

Current Project State:
| MC Version    | NeoForge        | Forge        | Fabric  |
| ------------- | --------------- | ------------ | ------- |
| 1.21.x        | 1.0-rc3         | -            | planned |
| 1.20.2-1.20.4 | 0.1             | -            | -       |
| 1.20.1        | -               | 0.1          | -       |
| 1.19.x        | -               | 0.1          | -       |
