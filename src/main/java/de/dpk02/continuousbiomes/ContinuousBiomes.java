package de.dpk02.continuousbiomes;

import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.repository.Pack;
import net.minecraft.server.packs.repository.PackSource;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.common.Mod;
import net.neoforged.neoforge.event.AddPackFindersEvent;

// The value here should match an entry in the META-INF/neoforge.mods.toml file
@Mod(ContinuousBiomes.MODID)
public class ContinuousBiomes
{
    // Define mod id in a common place for everything to reference
    public static final String MODID = "continuousbiomes";

    public ContinuousBiomes(IEventBus eventBus) {
        eventBus.addListener(ContinuousBiomes::addFeaturePacks);
    }

    public static void addFeaturePacks(final AddPackFindersEvent event) {
        event.addPackFinders(
                ResourceLocation.fromNamespaceAndPath("continuousbiomes", "data/worldgen"),
                PackType.SERVER_DATA,
                Component.literal("ContinuousBiomes - Example Worldgen"),
                PackSource.FEATURE,
                false,
                Pack.Position.TOP
        );
    }
}
