package de.dpk02.continuousbiomes.mixins;

import com.mojang.serialization.MapCodec;
import de.dpk02.continuousbiomes.lib.CBFunctions;
import net.minecraft.core.Registry;
import net.minecraft.world.level.levelgen.DensityFunction;
import net.minecraft.world.level.levelgen.DensityFunctions;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(DensityFunctions.class)
public class CBDensityFunctions {

	@Inject(
		method = "bootstrap",
		at = @At("RETURN")
	)
	private static void onBootstrap(Registry<MapCodec<? extends DensityFunction>> registry, CallbackInfoReturnable<MapCodec<? extends DensityFunction>> cir) {
		CBDensityFunctionsAccessor.register(registry, "x", CBFunctions.X.CODEC);
		CBDensityFunctionsAccessor.register(registry, "y", CBFunctions.Y.CODEC);
		CBDensityFunctionsAccessor.register(registry, "z", CBFunctions.Z.CODEC);
		CBDensityFunctionsAccessor.register(registry, "shifted_function", CBFunctions.ShiftedFunction.CODEC);
	}

}
