package de.dpk02.continuousbiomes.lib;

import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import de.dpk02.continuousbiomes.mixins.CBDensityFunctionsAccessor;
import net.minecraft.util.KeyDispatchDataCodec;
import net.minecraft.world.level.levelgen.DensityFunction;
import org.jetbrains.annotations.NotNull;

public class CBFunctions implements CBDensityFunctionsAccessor {

	public record X(double scale) implements DensityFunction.SimpleFunction {

		public static final KeyDispatchDataCodec<CBFunctions.X> CODEC = CBDensityFunctionsAccessor.makeCodec(
			RecordCodecBuilder.mapCodec(
				xInstance -> xInstance.group(
					Codec.doubleRange(
						-Double.MAX_VALUE,
						Double.MAX_VALUE
					)
					.fieldOf("scale").forGetter(CBFunctions.X::scale)
				)
				.apply(xInstance, CBFunctions.X::new)
			)
		);

		@Override
		public double compute(FunctionContext functionContext) {
			return functionContext.blockX() * scale;
		}

		@Override
		public double minValue() {
			return Double.MIN_VALUE;
		}

		@Override
		public double maxValue() {
			return Double.MAX_VALUE;
		}

		@Override
		public @NotNull KeyDispatchDataCodec<? extends DensityFunction> codec() {
			return CODEC;
		}
	}

	public record Y(double scale) implements DensityFunction.SimpleFunction {

		public static final KeyDispatchDataCodec<CBFunctions.Y> CODEC = CBDensityFunctionsAccessor.makeCodec(
			RecordCodecBuilder.mapCodec(
				xInstance -> xInstance.group(
					Codec.doubleRange(
						-Double.MAX_VALUE,
						Double.MAX_VALUE
					)
					.fieldOf("scale").forGetter(CBFunctions.Y::scale)
				)
				.apply(xInstance, CBFunctions.Y::new)
			)
		);

		@Override
		public double compute(FunctionContext functionContext) {
			return functionContext.blockY() * scale;
		}

		@Override
		public double minValue() {
			return Double.MIN_VALUE;
		}

		@Override
		public double maxValue() {
			return Double.MAX_VALUE;
		}

		@Override
		public @NotNull KeyDispatchDataCodec<? extends DensityFunction> codec() {
			return CODEC;
		}
	}

	public record Z(double scale) implements DensityFunction.SimpleFunction {

		public static final KeyDispatchDataCodec<CBFunctions.Z> CODEC = CBDensityFunctionsAccessor.makeCodec(
			RecordCodecBuilder.mapCodec(
				xInstance -> xInstance.group(
					Codec.doubleRange(
						-Double.MAX_VALUE,
						Double.MAX_VALUE
					)
					.fieldOf("scale").forGetter(CBFunctions.Z::scale)
				)
				.apply(xInstance, CBFunctions.Z::new)
			)
		);

		@Override
		public double compute(FunctionContext functionContext) {
			return functionContext.blockZ() * scale;
		}

		@Override
		public double minValue() {
			return Double.MIN_VALUE;
		}

		@Override
		public double maxValue() {
			return Double.MAX_VALUE;
		}

		@Override
		public @NotNull KeyDispatchDataCodec<? extends DensityFunction> codec() {
			return CODEC;
		}
	}



	public record ShiftedFunction(
		DensityFunction shiftX,
		DensityFunction shiftY,
		DensityFunction shiftZ,
		double xzScale,
		double yScale,
		DensityFunction input
	) implements DensityFunction {
		private static final MapCodec<CBFunctions.ShiftedFunction> DATA_CODEC = RecordCodecBuilder.mapCodec(
			p_208943_ -> p_208943_.group(
					DensityFunction.HOLDER_HELPER_CODEC.fieldOf("shift_x").forGetter(CBFunctions.ShiftedFunction::shiftX),
					DensityFunction.HOLDER_HELPER_CODEC.fieldOf("shift_y").forGetter(CBFunctions.ShiftedFunction::shiftY),
					DensityFunction.HOLDER_HELPER_CODEC.fieldOf("shift_z").forGetter(CBFunctions.ShiftedFunction::shiftZ),
					Codec.DOUBLE.fieldOf("xz_scale").forGetter(CBFunctions.ShiftedFunction::xzScale),
					Codec.DOUBLE.fieldOf("y_scale").forGetter(CBFunctions.ShiftedFunction::yScale),
					DensityFunction.HOLDER_HELPER_CODEC.fieldOf("input").forGetter(CBFunctions.ShiftedFunction::input)
				)
				.apply(p_208943_, CBFunctions.ShiftedFunction::new)
		);
		public static final KeyDispatchDataCodec<CBFunctions.ShiftedFunction> CODEC = CBDensityFunctionsAccessor.makeCodec(DATA_CODEC);

		@Override
		public double compute(DensityFunction.FunctionContext context) {
			int x = (int) ((double)context.blockX() * this.xzScale + this.shiftX.compute(context));
			int y = (int) ((double)context.blockY() * this.yScale + this.shiftY.compute(context));
			int z = (int) ((double)context.blockZ() * this.xzScale + this.shiftZ.compute(context));
			return this.input.compute(new FunctionContext() {
				@Override
				public int blockX() {
					return x;
				}

				@Override
				public int blockY() {
					return y;
				}

				@Override
				public int blockZ() {
					return z;
				}
			});
		}

		@Override
		public void fillArray(double @NotNull [] array, DensityFunction.ContextProvider contextProvider) {
			contextProvider.fillAllDirectly(array, this);
		}

		@Override
		public @NotNull DensityFunction mapAll(@NotNull DensityFunction.Visitor visitor) {
			return new ShiftedFunction(
				this.shiftX.mapAll(visitor),
				this.shiftY.mapAll(visitor),
				this.shiftZ.mapAll(visitor),
				this.xzScale,
				this.yScale,
				this.input.mapAll(visitor)
			);
		}

		@Override
		public double minValue() {
			return this.input.minValue();
		}

		@Override
		public double maxValue() {
			return this.input.maxValue();
		}

		@Override
		public @NotNull KeyDispatchDataCodec<? extends DensityFunction> codec() {
			return CODEC;
		}
	}
}
